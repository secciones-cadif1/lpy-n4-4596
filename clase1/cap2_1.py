# arreglo de valores strings
dias_semana = ["Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"]

print("los dias de la semana son:")
for dia in dias_semana:
    print(dia)

for i in range(7):
    print("dia",i+1,":",dias_semana[i])  

# cuando la cantidad de elementos de un arreglo es desconocida
# no debemos usar esta opcion para mostrar valores contenidos en el arreglo
#print("dia 1:",dias_semana[0])
#print("dia 2:",dias_semana[1])
#print("dia 3:",dias_semana[2])
#print("dia 4:",dias_semana[3])
#print("dia 5:",dias_semana[4])
#print("dia 6:",dias_semana[5])
#print("dia 7:",dias_semana[6])