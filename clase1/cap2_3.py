# se inicializa el arreglo vacío
nombres = [] 

i = 0
while True:
    i = i + 1
    print("Introduzca el nombre",i,":")
    nomb = input()
    # agrega un elemento al arreglo
    nombres.append(nomb) 

    print("Desea agregar otro nombre?")
    if input().upper() == "N":
        break

print("Los nombres leidos fueron:")
for nomb in nombres:
    print(nomb)