# practica de arreglos
# es un arreglo vacío
nombres = [] 

# arreglo de valores strings
dias_semana = ["Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"]

dia = "lunes" # es una variable normal, no es un arreglo

# primer elemento del arreglo
print("El primer dia de la semana es",dias_semana[0]) 
# último elemento del arreglo
print("Para algunos el primer dia de la semana es",dias_semana[6]) 

i = -2
print("El dia en la posición",i,"es",dias_semana[i])

# arreglo de valores enteros
edades = [15,35,28,40]

edad = 40 # no es un arreglo

print("las edades son",edades) # muestra todos los valores arreglo
#print("la edad es ",edades[6]) # produce un error
#print("la edad es ",edades[-5]) # produce un error

# se cambia el valor del elemento en la posicion 2 por el valor 
# almacenado en la variable "edad"
edades[2] = edad 
print("las edades son",edades) # muestra todos los valores arreglo