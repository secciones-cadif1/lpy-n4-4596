import random
# arreglo con 10 elementos
notas = [0,0,0,0,0,0,0,0,0,0]

# modulo que inicializa el arreglo con valores aleatorios
# en Python cuando arreglo es pasado por parámetro a un módulo,
# es pasado como parámetro por referencia, es decir, que los cambios
# que tenga el arreglo adentro del módulo, prevaleceran. No hace falta 
# return para devolver los cambios al arreglo
def inicializar_valores_aleatorios(notas):
    for i in range(10):
        notas[i] = random.randint(0,100)

# calcula y retorna el promedio de los valores del arreglo
def calcular_promedio_notas(notas):
    acum = 0
    for i in range(10):
        acum = acum + notas[i]
    
    return acum / 10

# función para contar cuantas notas estan por encima del promedio de notas
def contar_notas_mayores_promedio(promedio,notas):
    cont = 0
    for i in range(10):
        if notas[i] > promedio:
            cont = cont + 1
    
    return cont

def determinar_mayor_nota(notas):
    # se asume que el valor que esta en la posicion 0 es el mayor
    mayor = notas[0]
    # se recorre el arreglo desde la posición 1
    for i in range(1,10):
        if notas[i] > mayor:
            mayor = notas[i]
    
    return mayor

# cuenta la cantidad de notas menores a 50
def contar_notas_menores_50(notas):
    cont = 0
    for nota in notas:
        if nota < 50 :
            cont = cont + 1
            
    return cont

# cuerpo principal
inicializar_valores_aleatorios(notas)
print(notas)
prom = calcular_promedio_notas(notas)
mayor_nota = determinar_mayor_nota(notas)
cont_may_prom = contar_notas_mayores_promedio(prom,notas)
print("la mayor nota fue",mayor_nota)
print("el promedio de las notas es ",prom)
print("la cantidad de alumnos que tuvo nota mayor al promedio fue",cont_may_prom)


#leer notas y guardarlas en un arreglo
#for i in range(10):
#    print("Introduzca la nota del alumno",i+1)
#    notas[i] = int(input())

#print(notas)