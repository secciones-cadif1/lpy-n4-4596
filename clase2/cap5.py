# ambos arreglos estan relacionados entre ellos, es decir, son paralelos
# todos los arreglos deben el mismo tamaño
nombres = ["Jose","Pedro","Maria","Jesus"]
edades = [50,25,50,43]

def determinar_mayor_edad(edades):
    mayor = edades[0]
    for i in range(1,len(edades)):
        if edades[i] > mayor:
            mayor = edades[i]

    return mayor

def mostrar_estadisticas(edades,nombres):
    promedio = calcular_promedio_edades(edades)
    edad_mayor = determinar_mayor_edad(edades)

    print(len(edades),"alumnos registrados")
    print("El promedio de edades es",promedio)
    print("La mayor edad es",edad_mayor)

    # se muestran los nombre de los alumnos con edad igual a la mayor
    print("Los alumnos con edad igual al mayor son")
    for i in range(len(edades)):
        if edades[i] == edad_mayor:
            print(nombres[i])

    # se muestran los nombres de los alumnos con edad mayor al promedio
    print("Los alumnos con edad mayor al promedio son")
    for i in range(len(edades)):
        if edades[i] > promedio:
            print(nombres[i])

def calcular_promedio_edades(edades):
    # len es una funcion que retorna la longitud de un arreglo
    n = len(edades)
    if n == 0:
        return 0
    else:
        acum = 0
        for i in range(n): 
            acum = acum + edades[i]

        return acum / n

def mostrar_arreglos(nombres,edades):
    print("Los datos en los arreglos son")
    # se muestran los valores de los arreglos paralelos
    for i in range(4):
        print("El nombre ",i+1,"es",nombres[i]," y la edad es",edades[i])

def leer_arreglos(nombres,edades):
    for i in range(4):
        print("Introduzca el nombre",i+1)
        nombres[i] = input() 
        print("Introduzca la edad",i+1)
        edades[i] = int(input())

def mostrar_menu():
    print("=================")
    print("Opciones de Menu")
    print("=================")
    print("1. Leer datos")
    print("2. Mostrar datos")
    print("3. Ver estadisticas")
    print("4. Salir")
    print("")
    print("Selecciona una opcion")
    
    return int(input())

# cuerpo principal
while True:
    match mostrar_menu():
        case 1: 
            leer_arreglos(nombres,edades)
        case 2: 
            mostrar_arreglos(nombres,edades)
        case 3:
            mostrar_estadisticas(edades,nombres)
        case 4: 
            break
        case _:
            print("Opción incorrecta, intente de nuevo")

