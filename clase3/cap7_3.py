# algoritmo de busqueda de elementos NO repetidos
cedulas = ["123456","654321","987654","112233"]
nombres = ["Jose","Juan","Jose","Maria"]
apellidos = ["Rojas","Perez","Vivas","Rodriguez"]
cedBuscada = input("Introduzca la cédula a buscar ")

posicion = -1
for i in range(len(cedulas)):
    if cedulas[i] == cedBuscada:
        posicion = i
        break

if posicion == -1:
    print("No esta registrada la cedula",cedBuscada)
else:
    print("Ya esta registrada la cedula",cedBuscada,"y es de",
        nombres[posicion],apellidos[posicion])