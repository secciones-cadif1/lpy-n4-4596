# algoritmo de busqueda de elementos NO repetidos
cedulas = ["123456","654321","987654","112233"]
nombres = ["Jose","Juan","Jose","Maria"]
apellidos = ["Rojas","Perez","Vivas","Rodriguez"]
cedBuscada = input("Introduzca la cédula a buscar ")

posicion = -1
i = 0
while i < len(cedulas) and posicion == -1:
    if cedulas[i] == cedBuscada:
        posicion = i
    else:
        i = i + 1

if posicion == -1:
    print("No esta registrada la cedula",cedBuscada)
else:
    print("Ya esta registrada la cedula",cedBuscada,"y es de",
        nombres[posicion],apellidos[posicion])