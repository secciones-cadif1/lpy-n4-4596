# algoritmo de busqueda de elementos NO repetidos
cedulas = ["123456","654321","987654","112233"]
nombres = ["Jose","Juan","Jose","Maria"]

cedBuscada = input("Introduzca la cédula a buscar ")

encontrado = False
for i in range(len(cedulas)):
    if cedulas[i] == cedBuscada:
        encontrado = True
        break

if not encontrado:
    print("No esta registrada la cedula",cedBuscada)
else:
    print("Ya esta registrada la cedula",cedBuscada)