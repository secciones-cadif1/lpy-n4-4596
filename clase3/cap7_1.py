# algoritmo de busqueda de elementos repetidos
cedulas = ["123456","654321","987654","112233"]
nombres = ["Jose","Juan","Jose","Maria"]

nombreBuscado = input("Introduzca el nombre a buscar ")

cont = 0
for i in range(len(cedulas)):
    if nombres[i].upper() == nombreBuscado.upper():
        cont = cont + 1

if cont == 0:
    print("No hay coincidencias")
else:
    print("Hay",cont,"coincidencias")