def validar_numero_positivo(mensaje,error):
    # vamos a validar el dato
    while True:
        numero = int(input(mensaje))
        # se evalua si el valor cumple con el criterio de validación
        # si cumple, se rompe el ciclo. En caso contrario, se informa al usuario
        if numero > 0:
            return numero
        else:
            print(error+". Intente de nuevo")

def validar_numero_en_rango(mensaje,valor_min,valor_max,error):
    # vamos a validar el dato
    while True:
        numero = int(input(mensaje))
        # se evalua si el valor cumple con el criterio de validación
        # si cumple, se rompe el ciclo. En caso contrario, se informa al usuario
        if numero >= valor_min and numero <= valor_max:
            return numero
        else:
            print(error+". Intente de nuevo")

def validar_fecha(mensaje):
    while True:
        print(mensaje)
        dia = validar_numero_en_rango("Introduzca el dia",1,31,"El dia debe estar entre 1 y 31")
        mes = validar_numero_en_rango("Introduzca el mes:",1,12,"El mes debe estar entre 1 y 12")
        año = validar_numero_en_rango("Introduzca el año:",1950,2023,"El año debe estar entre 1950 y 2023")

        if (dia > 29 and mes == 2) or ((mes == 4 or mes == 6 or mes == 9 or mes == 11) and dia>30):
            print("fecha invalida. Intente de nuevo")
        else:
            return str(dia)+"/"+str(mes)+"/"+str(año)


fecha_nacimiento = validar_fecha("Introduzca la fecha de nacimiento:")

print("su fecha de nacimiento es "+fecha_nacimiento)

fecha_graduacion = validar_fecha("Introduzca la fecha de graduación de bachiller:")

edad = validar_numero_positivo("Introduzca la edad:","Debe ser un numero positivo")
estatura = validar_numero_positivo("Introduzca la estatura (cm):","Debe ser un número positivo")

dia = validar_numero_en_rango("Introduzca el dia",1,31,"El dia debe estar entre 1 y 31")
mes = validar_numero_en_rango("Introduzca el mes:",1,12,"El mes debe estar entre 1 y 12")
año = validar_numero_en_rango("Introduzca el año:",1950,2023,"El año debe estar entre 1950 y 2023")

print("La edad es:",edad)
if edad % 2 == 0:
    print("La edad es un número par")
else:
    print("La edad es un número impar")