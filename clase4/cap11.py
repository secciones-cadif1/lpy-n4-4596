# ambos arreglos estan relacionados entre ellos, es decir, son paralelos
# todos los arreglos deben el mismo tamaño
cedulas = ["123456","745124","852147","987654"]
nombres = ["Jose","Pedro","Maria","Jesus"]
apellidos = ["Rojas","Perez","Vivas","Rodriguez"]
edades = [50,25,50,43]

def validar_cadena(mensaje,long_minima,long_maxima,error):
    while True:
        print(mensaje)
        cadena = input()

        if len(cadena) >= long_minima and len(cadena) <= long_maxima:
            return cadena
        else:
            print(error)

# se verifica si el parámetro "cadena" tiene espacios en blanco
def tiene_espacios_en_blanco(cadena):
    for i in range(len(cadena)):
        if cadena[i] == " ":
            return True
    
    return False

def validar_nombre(mensaje,error):
    while True:
        nombre = validar_cadena(mensaje,3,100,error)
        
        if tiene_espacios_en_blanco(nombre):
            print("Debe introducir sólo el primer nombre")
        else:
            if nombre.isalpha():
                return nombre
            else:
                print("El nombre solo debe tener letras entre A-Z")

def validar_numero_en_rango(mensaje,valor_min,valor_max,error):
    # vamos a validar el dato
    while True:
        numero = input(mensaje)
        if not numero.isdigit():
            print("No es un número válido")
        else:
            numero = int(numero)
            # se evalua si el valor cumple con el criterio de validación
            # si cumple, se rompe el ciclo. En caso contrario, se informa al usuario
            if numero >= valor_min and numero <= valor_max:
                return numero
            else:
                print(error+". Intente de nuevo")

def intercambiar(arr,j):
    aux = arr[j]
    arr[j] = arr[j+1]
    arr[j+1] = aux

def ordernar_por_edad(cedulas,nombres,edades,apellidos):
    for i in range(len(edades)-1):
        for j in range(len(edades)-i-1):
            if edades[j] > edades[j+1]:
                intercambiar(edades,j)
                intercambiar(nombres,j)
                intercambiar(cedulas,j)
                intercambiar(apellidos,j)

def buscar_cedula(cedulas,ced_buscada):
    posicion = -1
    for i in range(len(cedulas)):
        if cedulas[i] == ced_buscada:
            posicion = i
            break

    return posicion

def consultar_datos(cedulas,nombres,edades,apellidos):
    print("Consulta de datos")
    print("=================")
    ced_buscada = input("Introduzca la cedula ")
    i = buscar_cedula(cedulas,ced_buscada)
    
    if i == -1:
        print("La cedula",ced_buscada,"no esta registrada")
    else:
        print("Esta registrado!!")
        print("Nombre: ",nombres[i])
        print("Apellido: ",apellidos[i])
        print("Edad: ",edades[i])

def determinar_mayor_edad(edades):
    mayor = edades[0]
    for i in range(1,len(edades)):
        if edades[i] > mayor:
            mayor = edades[i]

    return mayor

def mostrar_estadisticas(edades,nombres):
    promedio = calcular_promedio_edades(edades)
    edad_mayor = determinar_mayor_edad(edades)

    print(len(edades),"alumnos registrados")
    print("El promedio de edades es",promedio)
    print("La mayor edad es",edad_mayor)

    # se muestran los nombre de los alumnos con edad igual a la mayor
    print("Los alumnos con edad igual al mayor son")
    for i in range(len(edades)):
        if edades[i] == edad_mayor:
            print(nombres[i])

    # se muestran los nombres de los alumnos con edad mayor al promedio
    print("Los alumnos con edad mayor al promedio son")
    for i in range(len(edades)):
        if edades[i] > promedio:
            print(nombres[i])

def calcular_promedio_edades(edades):
    # len es una funcion que retorna la longitud de un arreglo
    n = len(edades)
    if n == 0:
        return 0
    else:
        acum = 0
        for i in range(n): 
            acum = acum + edades[i]

        return acum / n

def mostrar_arreglos(nombres,edades,cedulas,apellidos):
    print("Los datos en los arreglos son")
    # se muestran los valores de los arreglos paralelos
    print("# Cedula Nombre y Apellido      Edad")
    print("=====================================")
    for i in range(4):
        print(i+1,cedulas[i],"{0} {1:12}".format(nombres[i],apellidos[i]),
              "\t",edades[i])

def leer_arreglos(nombres,edades,cedulas,apellidos):
    for i in range(4):
        cedulas[i] = validar_numero_en_rango("Introduzca el número de cédula "+str(i+1)+":",1,99999999,"Cédula inválida")
        nombres[i] = validar_nombre("Introduzca el nombre "+str(i+1)+":","nombre inválido") 
        apellidos[i] = validar_nombre("Introduzca el apellido "+str(i+1)+":","apellido inválido") 
        edades[i] = validar_numero_en_rango("Introduzca la edad "+str(i+1)+":",1,99,"edad inválida")

def mostrar_menu():
    print("=================")
    print("Opciones de Menú")
    print("=================")
    print("1. Leer datos")
    print("2. Mostrar datos")
    print("3. Consultar datos")
    print("4. Ordenar por edad")
    print("5. Ver estadisticas")
    print("6. Salir")
    print("")
    print("Selecciona una opción")
    
    return int(validar_numero_en_rango("",1,6,"Opción invalida"))

# cuerpo principal
while True:
    match mostrar_menu():
        case 1: 
            leer_arreglos(nombres,edades,cedulas,apellidos)
        case 2: 
            mostrar_arreglos(nombres,edades,cedulas,apellidos)
        case 3:
            consultar_datos(cedulas,nombres,edades,apellidos)
        case 4:
            ordernar_por_edad(cedulas,nombres,edades,apellidos)
            print("Los datos fueron reordenados")
        case 5:
            mostrar_estadisticas(edades,nombres)
        case 6: 
            break
        case _:
            print("Opción incorrecta, intente de nuevo")

