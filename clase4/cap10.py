
def validar_numero_en_rango(mensaje,valor_min,valor_max,error):
    # vamos a validar el dato
    while True:
        numero = input(mensaje)
        if not numero.isdigit():
            print("No es un número válido")
        else:
            numero = int(numero)
            # se evalua si el valor cumple con el criterio de validación
            # si cumple, se rompe el ciclo. En caso contrario, se informa al usuario
            if numero >= valor_min and numero <= valor_max:
                return numero
            else:
                print(error+". Intente de nuevo")

def validar_tecla(mensaje,tecla1,tecla2,error):
    while True:
        print(mensaje)
        tecla = input().upper()

        if tecla == tecla1 or tecla == tecla2:
            return tecla
        else:
            print(error)

def validar_cadena(mensaje,long_minima,long_maxima,error):
    while True:
        print(mensaje)
        cadena = input()

        if len(cadena) >= long_minima and len(cadena) <= long_maxima:
            return cadena
        else:
            print(error)

# se verifica si el parámetro "cadena" tiene espacios en blanco
def tiene_espacios_en_blanco(cadena):
    for i in range(len(cadena)):
        if cadena[i] == " ":
            return True
    
    return False

def validar_nombre(mensaje,error):
    while True:
        nombre = validar_cadena(mensaje,3,100,error)
        
        if tiene_espacios_en_blanco(nombre):
            print("Debe introducir sólo el primer nombre")
        else:
            if nombre.isalpha():
                return nombre
            else:
                print("El nombre solo debe tener letras entre A-Z")

nombres = []
edades = [] # definimos un arreglo vacío
while True:
    # leemos una cadena válida
    nombre = validar_nombre("Introduzca el primer nombre:","El nombre no puede ser vacío")
    # leemos un número válido
    numero = validar_numero_en_rango("Introduzca la edad:",0,110,"La edad debe estar entre 0 y 110")

    nombres.append(nombre)
    # lo agregamos al arreglo
    edades.append(numero)
    # leemos la respuesta del usuario
    resp = validar_tecla("¿Desea agregar otro cliente?","S","N","opción invalida")

    if resp == "N":
        break